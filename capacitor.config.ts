import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'production-ready-frontend',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
