Production Ready (Frontend)
=======================================

[![Build Status](https://api.travis-ci.org/python/mypy.svg?branch=master)](https://travis-ci.org/python/mypy)

## 🧾 Contents
* [About the project](#)
* [Directory tree](#)
* [Getting Started](#)
  * [Prerequisites](#)
  * [Installing](#)
  * [Running the project](#)
* [Authors](#)

## 💭 About the project
"Production Ready" is an evaluation project for the technical interview by ItSide Company.

## 🌳 Directory tree
```text
├───public
│   └───assets
│       └───icon
└───src
    ├───components
    │   ├───CustomerAverageNote
    │   ├───CustomerList
    │   ├───EmptyCustomerList
    │   ├───ListPlaceholder
    │   └───NewCustomer
    ├───graphql
    │   └───queries
    ├───pages
    │   ├───Tab1
    │   ├───Tab2
    │   └───Tab3
    └───theme
```

## 🏁 Getting Started

#### 📋 Prerequisites
* You need [Git](https://git-scm.com/) and [Node.js](https://nodejs.org/) to install the project.

#### 📥 Installing
Node version must be 16 or higher.
```sh
> npm install
> npm install -g @ionic/cli 
```

#### 🌠 Running the project
Execute this command to run the project:
```
ionic serve
```

## 💼 Authors
* [**Zied Khechin**](https://www.linkedin.com/in/ziedkhechin/) - *Developer*

Project Repo: [https://gitlab.com/ziedkhechin/production-ready-frontend](https://gitlab.com/ziedkhechin/production-ready-frontend)
