import React from 'react';
import {IonList, IonRefresher, IonRefresherContent, RefresherEventDetail,} from '@ionic/react';
import {QueryResult, useQuery} from "@apollo/client";
import {GET_CUSTOMERS} from "../../graphql/queries/customers";
import {Customer, CustomerListItem} from "./CustomerListItem";
import {ListPlaceholder} from "../ListPlaceholder/ListPlaceholder";
import {EmptyCustomerList} from "../EmptyCustomerList/EmptyCustomerList";

/**
 * This component returns the list of customers
 * @constructor
 * @author Zied Khechin <ziedkhechin@ieee.org>
 */
export const CustomersList: React.FC = () => {
	const customers: QueryResult = useQuery(GET_CUSTOMERS, {notifyOnNetworkStatusChange: true,});
	/**
	 * This function reloads customers
	 * @param event
	 */
	const doRefresh = (event: CustomEvent<RefresherEventDetail>) => {
		customers.refetch().then(() => {
			event.detail.complete();
		});
	}

	if (customers.loading) {
		return <ListPlaceholder />;
	}

	return (
		<>
			<IonRefresher slot="fixed" onIonRefresh={doRefresh} pullFactor={0.5} pullMin={100} pullMax={200}>
				<IonRefresherContent></IonRefresherContent>
			</IonRefresher>
			{
				!!customers.data?.customers.data.length ? (
					<IonList>
						{customers.data?.customers.data.map((customer: Customer) => (
							<CustomerListItem key={customer.id} data={customer} />
						))}
					</IonList>
				)
				: <EmptyCustomerList />
			}
		</>
	)
};