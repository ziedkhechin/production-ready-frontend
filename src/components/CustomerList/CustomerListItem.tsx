import React from 'react';
import {IonAvatar, IonImg, IonItem, IonLabel} from "@ionic/react";
import PropTypes from 'prop-types';

export interface Customer {
	id: number,
	attributes: {
		email: string,
		note: number,
	}
}

/**
 * This component returns a single customer item of the list
 * @param data
 * @constructor
 * @author Zied Khechin <ziedkhechin@ieee.org>
 */
export const CustomerListItem: React.FC<any> = ({ data }) => {
	return (
		<IonItem>
			<IonAvatar slot="start">
				<IonImg src="assets/person-placeholder.png" />
			</IonAvatar>
			<IonLabel>
				<h2>{data.attributes.email}</h2>
				<p>Note: {data.attributes.note?.toFixed(2) ?? '0.00'}</p>
			</IonLabel>
		</IonItem>
	);
};
CustomerListItem.propTypes = {
	data: PropTypes.element.isRequired,
}