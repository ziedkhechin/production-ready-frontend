import React from "react";
import './CustomerAverageNote.scss';
import {IonRefresher, IonRefresherContent, RefresherEventDetail} from "@ionic/react";
import {QueryResult, useQuery} from "@apollo/client";
import {GET_CUSTOMER_NOTE_AVG} from "../../graphql/queries/customers";
import {ListPlaceholder} from "../ListPlaceholder/ListPlaceholder";

/**
 * This component returns the block of average of customers marks
 * @constructor
 * @author Zied Khechin <ziedkhechin@ieee.org>
 */
export const CustomerAverageNote = () => {
	const customerNoteAvg: QueryResult = useQuery(GET_CUSTOMER_NOTE_AVG, {notifyOnNetworkStatusChange: true,});
	/**
	 * This function reloads customers
	 * @param event
	 */
	const doRefresh = (event: CustomEvent<RefresherEventDetail>) => {
		customerNoteAvg.refetch().then(() => {
			event.detail.complete();
		});
	}

	if (customerNoteAvg.loading) {
		return <ListPlaceholder />;
	}

	return (
		<>
			<IonRefresher slot="fixed" onIonRefresh={doRefresh} pullFactor={0.5} pullMin={100} pullMax={200}>
				<IonRefresherContent></IonRefresherContent>
			</IonRefresher>
			<div className="avg-container">
				<p>The average of customers marks is :</p>
				<strong>{customerNoteAvg.data.customerNoteAvg.avg.toFixed(2)}</strong>
			</div>
		</>
	);
};