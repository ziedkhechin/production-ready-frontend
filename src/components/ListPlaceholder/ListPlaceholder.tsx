import {IonAvatar, IonItem, IonLabel, IonList, IonSkeletonText,} from "@ionic/react";

/**
 * This component returns the block of loading list
 * @constructor
 * @author Zied Khechin <ziedkhechin@ieee.org>
 */
export const ListPlaceholder = () => {
	return (
		<>
			<IonList>
				<IonItem>
					<IonAvatar slot="start">
						<IonSkeletonText animated />
					</IonAvatar>
					<IonLabel>
						<h3>
							<IonSkeletonText animated style={{ width: '50%' }} />
						</h3>
						<p>
							<IonSkeletonText animated style={{ width: '80%' }} />
						</p>
						<p>
							<IonSkeletonText animated style={{ width: '60%' }} />
						</p>
					</IonLabel>
				</IonItem>
				<IonItem>
					<IonAvatar slot="start">
						<IonSkeletonText animated />
					</IonAvatar>
					<IonLabel>
						<h3>
							<IonSkeletonText animated style={{ width: '50%' }} />
						</h3>
						<p>
							<IonSkeletonText animated style={{ width: '80%' }} />
						</p>
						<p>
							<IonSkeletonText animated style={{ width: '60%' }} />
						</p>
					</IonLabel>
				</IonItem>
				<IonItem>
					<IonAvatar slot="start">
						<IonSkeletonText animated />
					</IonAvatar>
					<IonLabel>
						<h3>
							<IonSkeletonText animated style={{ width: '50%' }} />
						</h3>
						<p>
							<IonSkeletonText animated style={{ width: '80%' }} />
						</p>
						<p>
							<IonSkeletonText animated style={{ width: '60%' }} />
						</p>
					</IonLabel>
				</IonItem>
				<IonItem>
					<IonAvatar slot="start">
						<IonSkeletonText animated />
					</IonAvatar>
					<IonLabel>
						<h3>
							<IonSkeletonText animated style={{ width: '50%' }} />
						</h3>
						<p>
							<IonSkeletonText animated style={{ width: '80%' }} />
						</p>
						<p>
							<IonSkeletonText animated style={{ width: '60%' }} />
						</p>
					</IonLabel>
				</IonItem>
			</IonList>
		</>
	);
};