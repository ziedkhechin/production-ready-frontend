import React, {useState} from 'react';
import {IonButton, IonButtons, IonContent, IonHeader, IonList, IonSearchbar, IonTitle, IonToolbar,} from '@ionic/react';
import {useQuery} from "@apollo/client";
import {FIND_CUSTOMER_BY_EMAIL} from "../../graphql/queries/customers";
import {Customer, CustomerListItem} from "../CustomerList/CustomerListItem";
import {ListPlaceholder} from "../ListPlaceholder/ListPlaceholder";
import {EmptyCustomerList} from "../EmptyCustomerList/EmptyCustomerList";

/**
 * This component returns the finding customers modal
 * @constructor
 * @author Zied Khechin <ziedkhechin@ieee.org>
 */
const FindCustomerModal = ({ modal }: any) => {
	const [searchEmail, setSearchEmail] = useState('');
	const customers = useQuery(FIND_CUSTOMER_BY_EMAIL, {
		variables: {email: searchEmail},
		skip: !searchEmail,
		notifyOnNetworkStatusChange: true,
	});
	/**
	 * This function closes the modal
	 */
	const dismiss = () => {
		modal.current?.dismiss();
	}

	return (
		<>
			<IonHeader>
				<IonToolbar>
					<IonTitle>Find Customer</IonTitle>
					<IonButtons slot="end">
						<IonButton onClick={() => dismiss()}>Close</IonButton>
					</IonButtons>
				</IonToolbar>
			</IonHeader>
			<IonContent className="ion-padding">
				<IonSearchbar
					value={searchEmail}
					onIonChange={e => setSearchEmail(e.detail.value!)}
					onClick={() => modal.current?.setCurrentBreakpoint(0.75)}
					placeholder="Type an email..."
				/>
				{
					customers.loading ?
						<ListPlaceholder />
						: !!customers.data?.customers.data.length ?
							(
								<IonList>
									{customers.data?.customers.data.map((customer: Customer) => (
										<CustomerListItem key={customer.id} data={customer} />
									))}
								</IonList>
							)
							: <EmptyCustomerList />
				}
			</IonContent>
		</>
	);
}

export default FindCustomerModal;