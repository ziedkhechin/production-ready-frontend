import React, {FunctionComponent, useState} from "react";
import {IonButton, IonInput, IonItem, IonLabel, useIonAlert,} from "@ionic/react";
import {CREATE_CUSTOMER} from "../../graphql/queries/customers";
import {useMutation} from "@apollo/client";

/**
 * This component returns the form of adding customer
 * @constructor
 * @author Zied Khechin <ziedkhechin@ieee.org>
 */
export const NewCustomerForm: FunctionComponent = () => {
	/**
	 * This function handles the success of the request
	 */
	const handleRequestSuccess = () => {
		present({
			header: 'Success',
			message: 'Customer has been saved successfully',
			buttons: [
				{ text: 'Ok' },
			],
		});
		setEmail('');
		setNote(null);
	};
	/**
	 * This function handles the failure of the request
	 * @param message
	 */
	const handleRequestError = (message: string) => {
		present({
			header: 'Error',
			message: message,
			buttons: [
				{ text: 'Ok' },
			],
		});
	};
	const [createCustomerMutation, createCustomer] = useMutation(CREATE_CUSTOMER, {
		refetchQueries: ['GetCustomers', 'GetCustomerNoteAvg'],
	});
	/**
	 * This function validates the form values
	 * @param values
	 */
	const validate = (values: { email: string; note: number|null; }) => {
		let errors: number = 0;

		if (!values.email || !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
			errors++;
		}
		if (values.note && (values.note < 0 || values.note > 20)) {
			errors++;
		}

		return errors;
	};
	const [email, setEmail] = useState('');
	const [note, setNote] = useState(null);
	const [present] = useIonAlert();
	/**
	 * This function handles the form submitting
	 */
	const handleSubmit = () => {
		if (!!validate({email, note})) {
			present({
				message: 'Please check your inputs and retry',
				buttons: [
					{ text: 'Ok' },
				],
			});
		} else {
			createCustomerMutation({
				variables: {
					email,
					note: Number.parseInt(note ?? '0', 10),
				},
				onCompleted: () => handleRequestSuccess(),
				onError: (error: { message: string; }) => handleRequestError(error.message),
			});
		}
	};

	return (
		<div className="ion-padding">
			<IonItem>
				<IonLabel position="floating">Email</IonLabel>
				<IonInput
					id="email"
					type="email"
					onIonChange={(e: CustomEvent) => setEmail(e.detail.value)}
					value={email}
					disabled={createCustomer.loading}
				/>
			</IonItem>
			<IonItem>
				<IonLabel position="floating">Note</IonLabel>
				<IonInput
					id="note"
					type="number"
					min="0"
					max="20"
					onIonChange={(e: CustomEvent) => setNote(e.detail.value)}
					value={note}
					disabled={createCustomer.loading}
				/>
			</IonItem>
			<IonButton className="ion-margin-top" expand="block" onClick={() => handleSubmit()}>Save</IonButton>
		</div>
	)
};