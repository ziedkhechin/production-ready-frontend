import React from "react";
import './EmptyCustomerList.scss'
import {personRemoveSharp} from "ionicons/icons";
import {IonIcon} from "@ionic/react";

/**
 * This component returns the block of an empty result
 * @constructor
 * @author Zied Khechin <ziedkhechin@ieee.org>
 */
export const EmptyCustomerList = () => {
	return (
		<div className="empty-container">
			<IonIcon icon={personRemoveSharp} />
			<strong>No customer found !</strong>
		</div>
	);
};