import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Tab3.css';
import {CustomerAverageNote} from "../../components/CustomerAverageNote/CustomerAverageNote";

const Tab3: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Average of notes</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Average of notes</IonTitle>
          </IonToolbar>
        </IonHeader>
          <CustomerAverageNote />
      </IonContent>
    </IonPage>
  );
};

export default Tab3;
