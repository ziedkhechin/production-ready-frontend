import {
    IonContent,
    IonFab,
    IonFabButton,
    IonHeader,
    IonIcon,
    IonModal,
    IonPage,
    IonTitle,
    IonToolbar
} from '@ionic/react';
import './Tab1.css';
import {CustomersList} from "../../components/CustomerList/CustomersList";
import {search} from "ionicons/icons";
import {useRef} from "react";
import FindCustomerModal from "../../components/FindCustomerModal/FindCustomerModal";

const Tab1: React.FC = () => {
    const modal = useRef<HTMLIonModalElement>(null);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Customers List</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">Customers List</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <CustomersList />
                <IonFab vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton id="open-modal">
                        <IonIcon icon={search} />
                    </IonFabButton>
                </IonFab>
                <IonModal ref={modal} trigger="open-modal">
                    <FindCustomerModal modal={modal} />
                </IonModal>
            </IonContent>
        </IonPage>
    );
};

export default Tab1;
