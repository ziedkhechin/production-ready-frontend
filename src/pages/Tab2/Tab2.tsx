import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Tab2.css';
import {NewCustomerForm} from "../../components/NewCustomer/NewCustomerForm";

const Tab2: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>New Customer</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">New</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <NewCustomerForm />
            </IonContent>
        </IonPage>
    );
};

export default Tab2;
