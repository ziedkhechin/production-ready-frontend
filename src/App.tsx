import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupIonicReact
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import {personAdd, people, calculator} from 'ionicons/icons';
import Tab1 from './pages/Tab1/Tab1';
import Tab2 from './pages/Tab2/Tab2';
import Tab3 from './pages/Tab3/Tab3';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route exact path="/customers/list">
            <Tab1 />
          </Route>
          <Route exact path="/customers/add">
            <Tab2 />
          </Route>
          <Route exact path="/customers/avg">
            <Tab3 />
          </Route>
          <Route exact path="/">
            <Redirect to="/customers/list" />
          </Route>
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton tab="tab1" href="/customers/list">
            <IonIcon icon={people} />
            <IonLabel>Customers</IonLabel>
          </IonTabButton>
          <IonTabButton tab="tab2" href="/customers/add">
            <IonIcon icon={personAdd} />
            <IonLabel>New</IonLabel>
          </IonTabButton>
          <IonTabButton tab="tab3" href="/customers/avg">
            <IonIcon icon={calculator} />
            <IonLabel>Average of notes</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
