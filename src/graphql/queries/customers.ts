import { gql } from '@apollo/client';

export const GET_CUSTOMERS = gql`
	query GetCustomers {
		customers (pagination: { pageSize: 100} ) {
			data {
				id
				attributes {
					email
					note
					createdAt
					updatedAt
				}
			}
		}
	}
`;

export const GET_CUSTOMER_NOTE_AVG = gql`
	query GetCustomerNoteAvg {
		customerNoteAvg {
			avg
		}
	}
`;

export const FIND_CUSTOMER_BY_EMAIL = gql`
	query FindCustomerByEmail($email: String) {
		customers(filters: { email: { startsWith: $email } }) {
			data {
				id
				attributes {
					email
					note
					createdAt
					updatedAt
				}
			}
		}
	}
`;

export const CREATE_CUSTOMER = gql`
	mutation CreateCustomer($email: String, $note: Float) {
		createCustomer(data: { email: $email, note: $note }) {
			data {
				id
			}
		}
	}
`;